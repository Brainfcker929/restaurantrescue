from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import View


class GeneratorView(View):
    template_name = "index/generator_index.html"

    def get(self, request):
        return render(request, self.template_name)
