from django.urls import path
from qrcode_generator.views import GeneratorView

urlpatterns = [
    path('', GeneratorView.as_view(), name='generator'),
]
