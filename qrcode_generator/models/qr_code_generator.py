import qrcode
from PIL import Image
import io
import base64


class QRCodeGenerator:
    def __init__(self, encripted_obj):
        self.show_qr_code = self.__generate(encripted_obj)

    def __generate(self, encripted_obj):
        encripted_string = encripted_obj.show_encryption
        img = qrcode.make(encripted_string)
        buf = io.BytesIO()
        img.save(buf, 'PNG')
        bild = self.__convertBase64(buf.getvalue())
        return bild.decode("utf-8")

    def __convertBase64(self, string):
        encoded = base64.b64encode(string)
        return encoded
