# RestaurantRescue
Hackathan 2020

## How to run the project local
First clone the repo:
```
cd in your projects directory
eg. home/user/school/projects/Hackathon
git clone git@gitlab.com:Brainfcker929/restaurantrescue.git
cd restaurantrescue/restaurantrescue
```
Install Python for Ubuntu

```
python3 ––version

```
If the revision level is lower than 3.7.x, or if Python is not installed, continue to the next step.

```
sudo apt update
sudo apt-get install python3
```

Install requirements
```
pip install
```

Then start the server

```
python3 manage.py runserver
```

Call this url to test if everything is working

```
http://localhost:8000/website/
```

## Developers:
> Dennis Hartenfels, Djalal Hajiyev, Paul Rusch
