from django.db import models
import datetime


class RestaurantUser(models.Model):
    restaurant_name = models.CharField(
        max_length=200, default="restaurant")
    encrypted_code = models.CharField(max_length=9998)
    time_of_arrival = models.DateTimeField(blank=True, null=True,
                                           default=datetime.datetime.now)
