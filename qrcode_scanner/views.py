from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import View
from .forms import RestaurantUserForm


class ScannerView(View):
    template_name = "index/scanner_index.html"

    def get(self, request):
        form = RestaurantUserForm()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request):
        form = RestaurantUserForm(request.POST)

        if form.is_valid():
            form.save()
            return render(request, 'index/scanner_success.html')

        return render(request, 'index/website_index.html', {'form': form})
