from django.urls import path
from qrcode_scanner.views import ScannerView

urlpatterns = [
    path('', ScannerView.as_view(), name='scanner'),
]
