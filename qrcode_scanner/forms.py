from django import forms
from django.forms import ModelForm
from qrcode_scanner.models.models import RestaurantUser


class RestaurantUserForm(forms.ModelForm):
    class Meta:
        model = RestaurantUser
        fields = ('__all__')
