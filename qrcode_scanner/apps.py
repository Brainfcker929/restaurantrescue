from django.apps import AppConfig


class QrcodeScannerConfig(AppConfig):
    name = 'qrcode_scanner'
