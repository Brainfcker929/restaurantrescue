from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from django.views import View

from .forms import NameForm
from .models.encryption import Encryption
from .models.decryption import Decryption
from qrcode_generator.models.qr_code_generator import QRCodeGenerator
from qrcode_scanner.models.models import RestaurantUser


class WebsiteView(View):
    template_name = "index/website_index.html"

    def get(self, request):
        form = NameForm()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request):
        form = NameForm(request.POST)

        if form.is_valid():
            encryption = Encryption(form)
            qr_code = QRCodeGenerator(encryption)
            context = {'qr_code': qr_code}
            return render(request, 'index/generator_index.html', context)

        return redirect(request, self.template_name, {'form': form})


class GesundheitsamtView(View):
    template_name = "index/gesundheitsamt.html"

    def get(self, request):
        encrypted_users = RestaurantUser.objects.all()

        customers_list = []

        for encrypted_user in encrypted_users:
            restaurant = encrypted_user.restaurant_name
            decrypted_obj = Decryption(encrypted_user)
            time_of_arrival = encrypted_user.time_of_arrival
            test = {
                "restaurant": restaurant,
                "first_name": decrypted_obj.info['First Name'],
                "last_name": decrypted_obj.info['Last Name'],
                "email": decrypted_obj.info['E-Mail'],
                "address": decrypted_obj.info['Address'],
                "city": decrypted_obj.info['City'],
                "zip_code": decrypted_obj.info['Zip Code'],
                "time_of_arrival": time_of_arrival,
            }
            customers_list.append(test)

        context = {'customers': customers_list}
        return render(request, self.template_name, context)
