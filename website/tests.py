from django.test import TestCase
import unittest


class Greeter:
    def __init__(self):
        self.message = 'Hello world!'
        print(self.message)


if __name__ == '__main__':
    greeter = Greeter()
    message = greeter.message
    print(message)


class MyTestCase(unittest.TestCase):
    def test_greeting(self):
        greeter = Greeter()
        self.assertEqual(greeter.message, 'Hello world!')


if __name__ == '__main__':
    unittest.main()
