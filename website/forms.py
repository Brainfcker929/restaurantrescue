from django import forms
from django.forms import ModelForm


class NameForm(forms.Form):
    first_name = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'id': 'inputFirstname', 'placeholder': 'First Name'}))

    last_name = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'id': 'inputLastname', 'placeholder': 'Last Name'}))

    email = forms.EmailField(required=True, widget=forms.EmailInput(
        attrs={'class': 'form-control', 'id': 'inputEmail4', 'placeholder': 'E-Mail'}))

    address = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'id': 'inputAddress', 'placeholder': 'Musterstr. 11'}))

    city = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'id': 'inputCity', 'placeholder': 'City'}))

    zip_code = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'id': 'inputPlz', 'placeholder': 'Zip-Code'}))
