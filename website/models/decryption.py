from cryptography.fernet import Fernet
import json
import environ


class Decryption:

    def __init__(self, restaurantuser):
        self.info = self.decrypt(restaurantuser)

    def decrypt(self, restaurantuser):
        key = self.load_key()
        f = Fernet(key)
        restaurantuser.encrypted_code
        encoded_crypted_byte = restaurantuser.encrypted_code.encode('utf-8')
        encoded_decrypted_byte = f.decrypt(encoded_crypted_byte)
        user_information = json.loads(encoded_decrypted_byte.decode('utf-8'))
        return user_information

    def load_key(self):
        env = environ.Env()
        environ.Env.read_env()
        key = env("FERNET_KEY")
        return key
