from cryptography.fernet import Fernet
import json
import environ


class Encryption:
    def __init__(self, form):
        self.show_encryption = self.encrypt(form)

    def encrypt(self, form):
        key = self.__load_key()
        cipher_suite = Fernet(key)
        user_information = self.__process_data(form)
        cipher_text = cipher_suite.encrypt(user_information)

        return cipher_text

    def __process_data(self, form):
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        email = form.cleaned_data['email']
        address = form.cleaned_data['address']
        city = form.cleaned_data['city']
        zip_code = form.cleaned_data['zip_code']
        user_information = {"First Name": first_name,
                            "Last Name": last_name,
                            "E-Mail": email,
                            "Address": address,
                            "City": city,
                            "Zip Code": zip_code}

        return json.dumps(user_information).encode('utf-8')

    def __load_key(self):
        env = environ.Env()
        environ.Env.read_env()
        key = env("FERNET_KEY")
        return key
