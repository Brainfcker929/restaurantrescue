# Generated by Django 3.1.3 on 2020-11-19 21:44

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Adress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('street', models.CharField(max_length=50)),
                ('zip_code', models.PositiveSmallIntegerField(unique=True, validators=[django.core.validators.MaxValueValidator(99999)])),
            ],
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=20)),
                ('last_name', models.CharField(max_length=20)),
                ('phonenumber', models.CharField(max_length=15, unique=True)),
                ('email', models.EmailField(max_length=254)),
                ('adress', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='website.adress')),
            ],
        ),
        migrations.AddField(
            model_name='adress',
            name='city',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='website.city'),
        ),
        migrations.AddField(
            model_name='adress',
            name='country',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='website.country'),
        ),
        migrations.AddField(
            model_name='adress',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='website.state'),
        ),
    ]
